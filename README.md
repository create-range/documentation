<H1>Cyber Research Environment And Threat Evaluation Range</H1>

<em>The system and information detailed below is the property of Virginia Polytechnic Institute and State University. Use of the referenced system implies agreement to the university’s Acceptable Use Policy (Policy 7000). For more information, please visit: https://vt.edu/acceptable-use.html</em>

**Table of Contents**

[[_TOC_]]

### CREATE Range Overview

The Virginia Tech National Security Institute’s Cyber Research Environment and Threat Evaluation (**CREATE**) Range is built upon Peraton Labs’ Cyber Virtual Assure Network ( [CyberVAN](https://www.peratonlabs.com/cybervan.html) ) platform to allow for a, “*...high-fidelity network environment for cyber experimentation, operational planning, validation and training*” [^1]. 

The range hardware is designed to support up to 1000 virtual machines across one more simultaneous scenarios and can also support hard ware in the loop capacity to bring in technologies such as 5G antennas and Internet of Things (IoT) devices. In order to more realistically represent high fidelity networks, CyberVAN incorporates latency, link capacity and routing protocols; a graphical cyber management interface to control the environment; a substantial cyber effects library including malware and vulnerable virtual machines; and realistic user traffic based on synthetic users [^1]. 

### Why A NSI Cyber Range?

+ A place for large scale research and student exercises 
   - The NSI Cyber Range is designed for large scale (up to 1000 VM) research and exercise environments

+ The NSI Cyber Range allows hardware in the loop integration
   - Expected IOC Summer Semester 2023

+ The NSI Cyber Range allows for VM resource scalability based on the research or exercise scenario

## Getting Started

<em>**Note**: You should have been provided with initial user credentials to log in to the CREATE Range. If you have not received that information, or if you have any other issues, questions, or comments, please send an e-mail to: **support@create-range.atlassian.net**</em>

- [x] After logging in for the first time, it is recommended to update your Apache Guacamole and Linux passwords (described further below, under [Update Guacamole Password](#Update Guacamole Password) and [Update TMC Linux Password](#Update TMC Linux Password))


## Accessing CyberVAN

We currently utilize Apache Guacamole for providing user access to the CyberVAN Testbed Management Controller (TMC). Guacamole consists of a web-based portal where users are able to securely login and interact, via a graphical remote desktop session, with the CyberVAN software suite, hosted on the TMC. Once connected, users are able to define, deploy, and launch scenarios for cyber security, research, as well as perform other testing consisting of virtual systems and networks.

You may access the CyberVAN Management Console by logging into Apache Guacamole, using the following CREATE Apache Guacamole Portal URL:

- https://portal.create.hume.vt.edu:8443

When you log in for the first time, you will be prompted to set up TOTP two-factor authentication. You will need to use a two-factor application, such as Google Authenticator or DUO Authenticator, to scan the QR code, and then enter the 6 digit code in the "Authentication Code" field, and press <kbd>Continue</kbd>.

<em>**Note**: This is an example of what you will see, when logging in for the first time:

![image.png](./images/image.png){width=60% height=60%}

After pressing continue, you will be placed into your remote session, on the Testbed Management Controller. 

<em>**Note**: Subsequent logins will place you in the `Home` screen of Apache Guacamole.</em>

## Launching CyberVAN
From the Desktop, you can launch the main features of CyberVAN:
+ **CyberVAN Management GUI** (Used for management of scenario’s, such as deploying, starting, gathering logs…)
+ **CyberVAN Scenario GUI** (Used for defining scenario’s, which are comprised of virtual network and system components, and represent the virtual network infrastructure)
+ **Xterm** (used for command line specific requirements on the TMC)

You can do this by right-clicking on the desktop, after which, you will see a menu appear:
 
 ![image-2.png](./images/image-2.png){width=60% height=60%}

## CyberVAN User Guide
For more information on using CyberVAN, please reference the **[CyberVAN User Guide](./documents/CyberVAN_TestbedUserGuideOctober2022_R5.0_FINAL.pdf)**


## Apache Guacamole Navigation
One important shortcut to know while logged into Guacamole is <kbd>Ctrl+Alt+Shift</kbd>, which will open/close the Guacamole sidebar menu, on the left-hand side of your screen.
 
From that menu you can disconnect from that current remote desktop session, return to the Guacamole Home Screen, or logout of Guacamole, for instance:
 
 ![image-3.png](./images/image-3.png){width=60% height=60%}
 
If you click <kbd>Home</kbd>, you will return to the home screen. 
 
 ![image-4.png](./images/image-4.png){width=60% height=60%}

 ![image-1.png](./images/image-1.png){width=60% height=60%}

Near the bottom of the Home screen you will see a list of Remote connections for which you have been granted access. Right now, you should see an available VNC connection under `ALL CONNECTIONS` named <em>yourusername-tmc01</em> which is your personal remote session to the TMC

If you click on the connection called <em>yourusername-tmc01</em>, it will open the remote connection to the TMC, and automatically place you back into your Linux user's virtual desktop instance. 
 
Also, should you disconnect or if your session time-outs or connection is lost, your remote desktop session will continue to run on the server, and you can log back into Guacamole to resume your work. You should see a graphical representation of your recent sessions upon logging back into Guacamole under `RECENT CONNECTIONS`, which you can click on, to resume that remote desktop session:
 
 ![image-5.png](./images/image-5.png){width=60% height=60%}

## Update Guacamole Password

After you have logged into Guacamole, you can change your Guacamole password by:
 
1.	Clicking on your username in the upper-right hand corner, and choosing <kbd>Settings</kbd>
2.	Clicking on the <kbd>Preferences</kbd> tab on the left hand side of the screen
3.	Enter your current/new password and clicking <kbd>Update Password</kbd>
 
 ![image-6.png](./images/image-6.png){width=60% height=60%}

 ## Update TMC Linux Password
Changing your password in Guacamole doesn’t affect your Linux password on the Testbed Management Controller. You can update your Linux password by running <kbd>passwd</kbd> within a XTerm terminal session, while logged into the TMC.


## Troubleshooting

Please e-mail **support@create-range.atlassian.net** if you experience any issues, have any questions, or have suggestions.

<br>

References:
[^1]: Peraton Labs. CyberVAN. https://www.peratonlabs.com/cybervan.html 
